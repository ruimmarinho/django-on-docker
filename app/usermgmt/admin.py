from django.contrib import admin
from .models import Profile, Hierarchy, ResetToken

admin.site.register(Profile)
admin.site.register(Hierarchy)
admin.site.register(ResetToken)