from django.db import models


class Profile(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=45)
    surname = models.CharField(max_length=45)
    title = models.CharField(max_length=45)
    email = models.CharField(max_length=45)
    password = models.TextField()
    phone = models.CharField(max_length=45)
    department = models.CharField(max_length=255, default=None, null=True)
    last_connection = models.DateTimeField(null=True, default=None)
    is_active = models.SmallIntegerField(default=0)
    status = models.SmallIntegerField(default=1)
    image_url = models.CharField(max_length=255, default="http://212.227.149.143:8000/static/profile_icon.png")


class Hierarchy(models.Model):
    id = models.AutoField(primary_key=True)
    subordinate = models.ForeignKey(Profile, on_delete=models.CASCADE, related_name='hierarchy_user_fk1')
    ordinate = models.ForeignKey(Profile, on_delete=models.CASCADE, related_name='hierarchy_user_fk2')


class AuthorizationToken(models.Model):
    token = models.TextField(max_length=255)
    fb_token = models.TextField(default=None, blank=True, null=True)
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)


class ResetToken(models.Model):
    token = models.CharField(max_length=255)
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)