from django.apps import AppConfig
from django.conf import settings


class UsermgmtConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'usermgmt'

    def ready(self):
        if settings.SCHEDULER_DEFAULT:
            from eagle_app import operator
            operator.start()