# Generated by Django 4.0.2 on 2022-02-28 12:21

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('usermgmt', '0003_authorizationtoken_delete_authorizationtokens_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='authorizationtoken',
            name='profile_id',
        ),
        migrations.RemoveField(
            model_name='resettoken',
            name='profile_id',
        ),
        migrations.AddField(
            model_name='authorizationtoken',
            name='profile',
            field=models.ForeignKey(default=-1.0, on_delete=django.db.models.deletion.CASCADE, to='usermgmt.profile'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='resettoken',
            name='profile',
            field=models.ForeignKey(default=-1.0, on_delete=django.db.models.deletion.CASCADE, to='usermgmt.profile'),
            preserve_default=False,
        ),
    ]
