from django.conf import settings as conf_settings
from usermgmt.models import AuthorizationToken
from datetime import datetime, timedelta
import jwt


class TokensView:
    def isAuth(request):
        # Get token on request
        token = request.META['HTTP_AUTHORIZATION']
        # Check if token exists
        if AuthorizationToken.objects.filter(token=token).exists():
            return True
        else:
            return False

    def removeTokensExpired(self):
        # Get all tokens
        tokens = AuthorizationToken.objects.all()
        for token in tokens:
            # Get time from token
            created_date = datetime.strptime(
                jwt.decode(token.token, conf_settings.JWT_KEY, algorithms=['HS256'])['create_date'],
                '%Y-%m-%d %H:%M:%S.%f')

            # Get the current date
            current_date = datetime.now()

            # Check if token has created more than 15 days
            if current_date >= (created_date + timedelta(days=15)):
                # Yes, delete token from database
                token_db = AuthorizationToken.objects.get(token=token.token)
                token_db.delete()

    def prueba(self):
        print("entra")
