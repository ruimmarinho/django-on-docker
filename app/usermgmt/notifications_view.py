from django.conf import settings as conf_settings
from usermgmt.models import Profile, AuthorizationToken, Hierarchy
import json


class SendNotifications():
    # Type:
    # 1 - Create Task
    # 2 - Update Info Task
    # 3 - Update Progress Task
    # 4 - Delete Task

    # Check who to notify
    @staticmethod
    def check(type_not, creator_id, user_id, task):

        user_lang = "en"
        json_data = open('./language/' + user_lang + '.json')
        lang = json.load(json_data)
        json_data.close()

        # Create
        if type_not == 1:
            if creator_id != user_id:
                user_tokens = AuthorizationToken.objects.filter(profile_id=user_id)
                # Send Notification to Owner
                SendNotifications().notify(
                    user_tokens,
                    lang["new_task_title"],
                    lang["new_task_body"].format(task.title),
                    'task',
                    'create',
                    task.id
                )

        # Update
        elif type_not == 2:
            if ((creator_id == task.creator_id) and (task.creator_id != user_id)) or (
                    (creator_id != task.creator_id) and (task.creator_id != user_id) and (creator_id != user_id)):
                # Send Notification to Owner
                user_tokens = AuthorizationToken.objects.filter(profile_id=user_id)
                SendNotifications().notify(
                    user_tokens,
                    lang["update_task_title"],
                    lang["update_task_body"].format(task.title),
                    'task',
                    'update',
                    task.id
                )

            elif (creator_id != task.creator_id) and (task.creator_id != user_id):
                # Send Notification to Superior
                superiors = Hierarchy.objects.filter(subordinate_id=user_id)
                for user in superiors:
                    user_tokens = AuthorizationToken.objects.filter(profile_id=user.ordinate_id)
                    SendNotifications().notify(
                        user_tokens,
                        lang["update_task_title"],
                        lang["update_task_body"].format(task.title),
                        'task',
                        'update',
                        task.id
                    )

        # Update Progress
        elif type_not == 3:
            if ((creator_id == task.creator_id) and (task.creator_id != user_id)) or (
                    (creator_id != task.creator_id) and (task.creator_id != user_id) and (creator_id != user_id)):
                # Send Notification to Owner
                user_tokens = AuthorizationToken.objects.filter(profile_id=user_id)
                SendNotifications().notify(
                    user_tokens,
                    lang["update_task_title"],
                    lang["update_task_owner_body"].format(task.title),
                    'task',
                    'update',
                    task.id
                )

            elif (creator_id != task.creator_id) and (task.creator_id != user_id):
                user = Profile.objects.get(id=user_id)
                user_name = user.name + ' ' + user.surname
                # Send Notification to Superior
                superiors = Hierarchy.objects.filter(subordinate_id=user_id)
                for user in superiors:
                    user_tokens = AuthorizationToken.objects.filter(profile_id=user.ordinate_id)
                    SendNotifications().notify(
                        user_tokens,
                        lang["update_task_title"],
                        lang["update_task_superior_body"].format(user_name),
                        'task',
                        'update',
                        task.id
                    )

        # Delete
        elif type_not == 4:
            if ((creator_id == task.creator_id) and (task.creator_id != user_id)) or (
                    (creator_id != task.creator_id) and (task.creator_id != user_id) and (creator_id != user_id)):
                # Send Notification to Owner
                user_tokens = AuthorizationToken.objects.filter(profile_id=user_id)
                SendNotifications().notify(
                    user_tokens,
                    lang["delete_task_title"],
                    lang["delete_task_owner_body"],
                    'task',
                    'delete',
                    task.id
                )

            elif (creator_id != task.creator_id) and (task.creator_id != user_id):
                user = Profile.objects.get(id=user_id)
                user_name = user.name + ' ' + user.surname
                # Send Notification to Superior
                superiors = Hierarchy.objects.filter(subordinate_id=user_id)
                for user in superiors:
                    user_tokens = AuthorizationToken.objects.filter(profile_id=user.ordinate_id)
                    SendNotifications().notify(
                        user_tokens,
                        lang["delete_task_title"],
                        lang["delete_task_superior_body"].format(user_name),
                        'task',
                        'delete',
                        task.id
                    )

    # Send notifications
    @staticmethod
    def notify(user_tokens, message_title, message_body, type_not, subtype_not, id_not):
        fb_tokens = []
        for token in user_tokens:
            fb_tokens.append(token.fb_token)

        # Firebase Token
        registration_ids = fb_tokens
        # Title and message for notification
        message_title = message_title
        message_body = message_body
        data_message = {
            "type": type_not,
            "subtype": subtype_not,
            "id": id_not
        }
        conf_settings.PUSH_SERVICE.notify_multiple_devices(
            registration_ids=registration_ids,
            data_message=data_message,
            message_title=message_title,
            message_body=message_body
        )