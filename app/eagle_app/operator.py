from apscheduler.schedulers.background import BackgroundScheduler
from django_apscheduler.jobstores import register_events, DjangoJobStore
from usermgmt.tokens_view import TokensView


def start():
    scheduler = BackgroundScheduler()
    # scheduler.add_jobstore(DjangoJobStore(), 'djangojobstore')
    # register_events(scheduler)

    # This function is used to remove expired tokens
    @scheduler.scheduled_job('interval', seconds=120, name='remove_expired_tokens')
    def remove_expired_tokens():
        t = TokensView()
        t.removeTokensExpired()


    # @scheduler.scheduled_job('cron', hour=13, minute=37, name='pending_block_activity_notification')
    # def pending_block_activity_notification():
    #     print("pending_block_activity_notification Job started")

    scheduler.start()
