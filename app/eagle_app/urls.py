from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('user-mgmt/', include('usermgmt.urls')),
    path('task-mgmt/', include('taskmgmt.urls')),
    path('backend/', include('backend.urls')),
    path('admin/', admin.site.urls),
]
