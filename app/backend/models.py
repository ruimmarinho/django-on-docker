from django.db import models
from usermgmt.models import Profile


class Notifications(models.Model):
    id = models.AutoField(primary_key=True)
    user_id = models.ForeignKey(Profile, on_delete=models.CASCADE)
    code = models.SmallIntegerField()
    status = models.SmallIntegerField()
    attributes = models.JSONField()




