from django.urls import path
from . import views

urlpatterns = [

    path('', views.index, name='index'),
    path('user/<int:user_id>/today/', views.GetTodayTasks.as_view()),
    path('user/<int:user_id>/date/<str:date>/', views.GetTasksByDate.as_view()),
    path('user/<int:user_id>/ongoing/', views.GetOngoingTasks.as_view()),
    path('user/<int:user_id>/completed/<int:n_tasks>/', views.GetCompletedTasks.as_view()),
    path('user/<int:user_id>/tasks/', views.CreateTask.as_view()),
    path('user/<int:user_id>/tasks/<int:task_id>/', views.CreateTask.as_view()),
]