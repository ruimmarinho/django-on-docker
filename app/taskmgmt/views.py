from django.http import HttpResponse
from rest_framework.views import APIView
from django.conf import settings as conf_settings
from usermgmt.tokens_view import TokensView

from .models import Task
from django.db.models import Q
import datetime
import json
import jwt

from usermgmt.notifications_view import SendNotifications


def index(request):
    return HttpResponse("Welcome to TaskManagement")


# This class returns today tasks of a given user
class GetTodayTasks(APIView):

    def get(self, request, user_id=-1, pk=None):
        # Check authentication token
        if TokensView.isAuth(request):
            # Obtain the user data, checking if the user is the own user (the authenticated) or
            # otherwise, the user is another user which data is taken from the database
            try:
                # Get tasks from database
                today_min = datetime.datetime.combine(datetime.date.today(), datetime.time.min)
                today_max = datetime.datetime.combine(datetime.date.today(), datetime.time.max)

                tasks = list(Task.objects.filter(owner=user_id).filter(
                    Q(Q(start_date__lte=today_min) & Q(percentage__lt=100)) |
                    Q(Q(end_date__range=(today_min, today_max)) & Q(percentage__lt=100)) |
                    Q(Q(percentage__lt=100) & Q(end_date__lt=today_min)) |
                    Q(Q(start_date__range=(today_min, today_max)) & Q(percentage__lt=100))))

                # Check the user has tasks
                if len(tasks) > 0:

                    # Creating response list
                    responseList = []

                    # Iterating list of tasks in order to create the response list
                    for task in tasks:
                        try:
                            d = task.end_date
                            s = d.timestamp()
                            responseList.append(FormatTask().format(task))
                        except Exception as e:
                            a = e

                    # Create the response
                    return HttpResponse(json.dumps(responseList), status=200, content_type="application/json")

                else:
                    # Return a 200 with empty response
                    return HttpResponse('[]', status=200, content_type="application/json")

            except (Task.DoesNotExist):
                # Return a 404 unauthorized error
                return HttpResponse('User not found', status=404)

        else:
            return HttpResponse('Unauthorized', status=401)


# This class returns today tasks of a given user
class GetTasksByDate(APIView):
    def get(self, request, user_id=-1, date=-1, pk=None):
        # Check authentication token
        if TokensView.isAuth(request):
            try:

                # Create date min and max
                date_min_str = date + " 00:00:00"
                date_max_str = date + " 23:59:59"

                # Get tasks from database
                date_min = datetime.datetime.strptime(date_min_str, '%Y-%m-%d %H:%M:%S')
                date_max = datetime.datetime.strptime(date_max_str, '%Y-%m-%d %H:%M:%S')
                tasks = list(Task.objects.filter(owner=user_id).filter(end_date__range=(date_min, date_max)))

                # Check the user has tasks
                if len(tasks) > 0:

                    # Creating response list
                    responseList = []

                    # Iterating list of tasks in order to create the response list
                    for task in tasks:
                        responseList.append(FormatTask().format(task))

                    # Create the response
                    return HttpResponse(json.dumps(responseList), status=200, content_type="application/json")

                else:
                    # Return a 200 with empty response
                    return HttpResponse('[]', status=200, content_type="application/json")

            except (Task.DoesNotExist):
                # Return a 404 unauthorized error
                return HttpResponse('User not found', status=404)

        else:
            return HttpResponse('Unauthorized', status=401)


# This class returns ongoing tasks of a given user
class GetOngoingTasks(APIView):
    def get(self, request, user_id=-1, pk=None):
        # Check authentication token
        if TokensView.isAuth(request):
            try:

                # Get next Monday in DateTime
                thisMondayDateTime = WeekShorcuts().getThisMondayDateTime()
                nextMondayDateTime = WeekShorcuts().getNextMondayDateTime()

                # Get Now in DateTime
                nowDateTime = datetime.datetime.now()

                # Get tasks that end this week
                tasks = list(Task.objects.filter(owner=user_id).filter(
                    Q(end_date__range=(thisMondayDateTime, nextMondayDateTime)) |
                    Q(Q(start_date__lt=nowDateTime) & Q(percentage__lt=100)) |
                    Q(Q(end_date__lt=nowDateTime) & Q(percentage__lt=100)))
                )

                # Check the user has tasks
                if len(tasks) > 0:

                    # Creating response list
                    responseList = []

                    # Iterating list of tasks in order to create the response list
                    for task in tasks:
                        responseList.append(FormatTask().format(task))

                    # Create the response
                    return HttpResponse(json.dumps(responseList), status=200, content_type="application/json")

                else:
                    # Return a 200 with empty response
                    return HttpResponse('[]', status=200, content_type="application/json")

            except (Task.DoesNotExist):
                # Return a 404 unauthorized error
                return HttpResponse('User not found', status=404)

        else:
            return HttpResponse('Unauthorized', status=401)


# This class returns completed tasks of a given user
class GetCompletedTasks(APIView):
    def get(self, request, user_id=-1, n_tasks=0, pk=None):
        # Check authentication token
        if TokensView.isAuth(request):
            try:
                # Get x(n_tasks) completed tasks, percentage = 100%
                tasks = list(Task.objects.filter(owner=user_id, percentage=100).order_by('-id')[:n_tasks])

                # Check the user has tasks
                if len(tasks) > 0:

                    # Creating response list
                    responseList = []

                    # Iterating list of tasks in order to create the response list
                    for task in tasks:
                        responseList.append(FormatTask().format(task))

                    # Create the response
                    return HttpResponse(json.dumps(responseList), status=200, content_type="application/json")

                else:
                    # Return a 200 with empty response
                    return HttpResponse('[]', status=200, content_type="application/json")

            except (Task.DoesNotExist):
                # Return a 404 unauthorized error
                return HttpResponse('User not found', status=404)

        else:
            return HttpResponse('Unauthorized', status=401)


# This class creates a new task for a defined user
class CreateTask(APIView):
    # GET Request to send task
    def get(self, request, user_id=-1, task_id=-1, pk=None):
        # Check authentication token, and the user_id has correct values
        if TokensView.isAuth(request) and user_id != -1 and task_id != -1:
            # Get user task from id
            try:
                task = Task.objects.get(id=task_id, owner_id=user_id)
            except Task.DoesNotExist:
                task = None

            if task is not None:
                jsonTask = FormatTask().format(task)
                # Return a 200 success message
                return HttpResponse(json.dumps(jsonTask), status=200,
                                    content_type="application/json")
            else:
                # Return a 404 Task not found
                return HttpResponse('Task not found', status=404)

        else:
            # Return a 401 unauthorized error
            return HttpResponse('Unauthorized', status=401)

    # POST Request to create a new task
    def post(self, request, user_id=-1, pk=None):
        # Check authentication token, and the user_id has correct values
        if TokensView.isAuth(request) and user_id != -1:
            # Obtain task data for the body of the request, which is a JSON
            json_body = json.loads(request.body)

            # Obtain the related token
            token = request.META['HTTP_AUTHORIZATION']
            # Obtain the id from user
            creator_id = jwt.decode(token, conf_settings.JWT_KEY, algorithms=['HS256'])['profile_id']

            # Create new task for the user
            task = Task(owner_id=user_id,
                        creator_id=creator_id,
                        title=json_body['title'],
                        description=json_body['description'],
                        start_date=datetime.datetime.fromtimestamp(json_body['start_date']),
                        end_date=datetime.datetime.fromtimestamp(json_body['end_date']),
                        percentage=0)
            task.save()

            # Check and send notification
            SendNotifications().check(1, creator_id, user_id, task)

            # Return a 200 success message
            # return HttpResponse('Task created successfully', status=200)
            return HttpResponse(json.dumps([{'result': 'Task created successfully'}, {'data': {'id': task.pk}}]),
                                status=200, content_type="application/json")

        else:
            # Return a 401 unauthorized error
            return HttpResponse('Unauthorized', status=401)

    # PATCH Request to modify an existing task
    def patch(self, request, user_id=-1, task_id=-1, pk=None):
        # Check authentication token, and the user_id has correct values
        if TokensView.isAuth(request) and user_id != -1 and task_id != -1:

            try:
                # Verify there is a task with the given id
                task = Task.objects.get(id=task_id)

                progress = task.percentage

                # Obtain task data for the body of the request, which is a JSON
                json_body = json.loads(request.body)
                if 'title' in json_body:
                    task.title = json_body['title']
                if 'description' in json_body:
                    task.description = json_body['description']
                if 'start_date' in json_body:
                    task.start_date = datetime.datetime.fromtimestamp(json_body['start_date'])
                if 'end_date' in json_body:
                    task.end_date = datetime.datetime.fromtimestamp(json_body['end_date'])
                if 'percentage' in json_body:
                    task.percentage = json_body['percentage']

                # Save task with the given changes
                task.save()

                # Obtain the related token
                token = request.META['HTTP_AUTHORIZATION']
                # Obtain the id from user
                creator_id = jwt.decode(token, conf_settings.JWT_KEY, algorithms=['HS256'])['profile_id']

                if progress == task.percentage:
                    SendNotifications().check(2, creator_id, user_id, task)
                else:
                    SendNotifications().check(3, creator_id, user_id, task)

                # Return a 200 success message
                return HttpResponse('Task modified successfully', status=200)

            except Task.DoesNotExist:
                # Return a 404 not found
                return HttpResponse('Not existing task with the given id', status=404)

        else:
            # Return a 401 unauthorized error
            return HttpResponse('Unauthorized', status=401)

    # DELETE Request to remove an existing task
    def delete(self, request, user_id=-1, task_id=-1, pk=None):
        # Check authentication token, and the user_id has correct values
        if TokensView.isAuth(request) and user_id != -1 and task_id != -1:

            try:
                # Verify there is a task with the given id
                task = Task.objects.get(id=task_id)

                # Delete existing task
                task.delete()

                # Send Notification
                SendNotifications().check(4, task.creator_id, user_id, task)

                # Return a 200 success message
                return HttpResponse('Task deleted successfully', status=200)

            except Task.DoesNotExist:
                # Return a 404 not found
                return HttpResponse('Not existing task with the given id', status=404)

        else:
            # Return a 401 unauthorized error
            return HttpResponse('Unauthorized', status=401)


class WeeklyTasks():

    # This method returns the number of tasks for this week
    @staticmethod
    def totalWeekTasks(user_id):
        # Get next Monday in DateTime
        thisMondayDateTime = WeekShorcuts().getThisMondayDateTime()
        nextMondayDateTime = WeekShorcuts().getNextMondayDateTime()

        # Get tasks that end this week
        # tasks = list(Task.objects.filter(owner=user_id).filter(
        #     end_date__range=(thisMondayDateTime, nextMondayDateTime)))

        tasks = list(Task.objects.filter(owner=user_id).filter(
            Q(start_date__lte=thisMondayDateTime) |
            Q(end_date__range=(thisMondayDateTime, nextMondayDateTime)) |
            Q(end_date__lt=thisMondayDateTime) |
            Q(start_date__range=(thisMondayDateTime, nextMondayDateTime))))

        return tasks


class WeekShorcuts():

    # This method returns next monday
    @staticmethod
    def getThisMondayDateTime():
        today = datetime.date.today()
        thisMonday = today - datetime.timedelta(days=today.weekday())
        thisMonday = str(thisMonday) + " 00:00:00"

        # Get datetime for next monday
        thisMondayDateTime = datetime.datetime.strptime(thisMonday, '%Y-%m-%d %H:%M:%S')

        return thisMondayDateTime

    # This method returns next monday
    @staticmethod
    def getNextMondayDateTime():
        today = datetime.date.today()
        thisMonday = today - datetime.timedelta(days=today.weekday())
        nextMonday = thisMonday + datetime.timedelta(weeks=1)
        nextMonday = str(nextMonday) + " 00:00:00"

        # Get datetime for next monday
        nextMondayDateTime = datetime.datetime.strptime(nextMonday, '%Y-%m-%d %H:%M:%S')

        return nextMondayDateTime


class FormatTask():

    # Status:
    #   0 - Verde
    #   1 - Amarillo
    #   2 - Rojo

    # This method formats the data of a task into a JSON Object
    @staticmethod
    def format(task):
        jsonObject = {'id': task.id,
                      'owner': task.owner.pk,
                      'start_date': int(task.start_date.timestamp()),
                      'start_date_str': str(task.start_date),
                      'end_date': int(task.end_date.timestamp()),
                      'end_date_str': str(task.end_date),
                      'title': task.title,
                      'description': task.description,
                      'percentage': task.percentage,
                      'status': 0
                      }

        return jsonObject
