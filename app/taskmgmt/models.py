from django.db import models
from usermgmt.models import Profile


class Task(models.Model):
    id = models.AutoField(primary_key=True)
    owner = models.ForeignKey(Profile, on_delete=models.CASCADE, related_name='profile_fk1')
    creator = models.ForeignKey(Profile, on_delete=models.CASCADE, related_name='profile_fk2')
    created_date = models.DateTimeField(auto_now_add=True)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    title = models.CharField(max_length=120)
    description = models.TextField()
    percentage = models.SmallIntegerField(default=0)
    notifications = models.JSONField(default=dict)
